package com.so.work.common.model;

public class ResultModel {
	public boolean success;
	public Object result;
	public String massage;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public String getMassage() {
		return massage;
	}
	public void setMassage(String massage) {
		this.massage = massage;
	}
	
	@Override
	public String toString() {
		return "ResultModel [success=" + success + ", result=" + result + ", massage=" + massage + "]";
	}
	
}
