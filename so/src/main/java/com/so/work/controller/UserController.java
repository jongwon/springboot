package com.so.work.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.so.work.common.model.ResultModel;
import com.so.work.model.UserModel;
import com.so.work.service.BoardService;
import com.so.work.service.JwtService;
import com.so.work.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	BoardService boardService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	JwtService jwtService;
	
	
	/**
     * user list
     * @author jongwon
     * @param 
     * @return
     * 사용자 목록 조회
     */
	@RequestMapping("/list")
	public List<UserModel> boardList() {
		List<UserModel> userList = new ArrayList<UserModel>();
		try {
			userList = userService.userList();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return userList;
	}
	
	/**
     * login user
     * @author jongwon
     * @param 
     * @return
     * postman test 
     * raw / JSON(application/json)
     * JWT 예제 
     */
    @RequestMapping(value="/loginUser", method = RequestMethod.POST)
	public ResultModel signin(UserModel param, HttpServletResponse response) {
		ResultModel result = new ResultModel();
		
		try {
			UserModel loginMember = userService.loginUser(param);
			String token = jwtService.create("member", loginMember, "user");
			
			response.setHeader("Authorization", token);
			result.setResult(token);
			result.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

}
