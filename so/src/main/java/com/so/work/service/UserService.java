package com.so.work.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.so.work.mapper.UserMapper;
import com.so.work.model.UserModel;

@Service
public class UserService {
	
	@Autowired
	private UserMapper userMapper;

	public List<UserModel> userList() throws Exception{
        return userMapper.userList();
    }
	
	public int insertUser(UserModel param) throws Exception{
        return userMapper.insertUser(param);
    }
	
	public UserModel loginUser(UserModel param) throws Exception{
		return param;
    }

}
