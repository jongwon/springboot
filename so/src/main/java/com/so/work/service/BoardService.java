package com.so.work.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.so.work.mapper.BoardMapper;
import com.so.work.model.BoardModel;

@Service
public class BoardService {
	
	@Autowired
	private BoardMapper boardMapper;

	public List<BoardModel> boardList() throws Exception{
        return boardMapper.boardList();
    }


}
