package com.so.work.model;

public class UserModel {
	public String userId;
	public String userPw;
	public String userEmail;
	public String userLike;
	public String userAddr;
	public String regDt;
	public String modDt;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserLike() {
		return userLike;
	}
	public void setUserLike(String userLike) {
		this.userLike = userLike;
	}
	public String getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getModDt() {
		return modDt;
	}
	public void setModDt(String modDt) {
		this.modDt = modDt;
	}
	@Override
	public String toString() {
		return "UserModel [userId=" + userId + ", userPw=" + userPw + ", userEmail=" + userEmail + ", userLike="
				+ userLike + ", userAddr=" + userAddr + ", regDt=" + regDt + ", modDt=" + modDt + "]";
	}
	
}
