package com.so.work.model;

public class BoardModel {
	public String idx;
	public String subject;
	public String contents;
	public String regDt;
	public String userId;
	public String etc;
	public Integer hit;
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public Integer getHit() {
		return hit;
	}
	public void setHit(Integer hit) {
		this.hit = hit;
	}
	
	@Override
	public String toString() {
		return "BoardModel [idx=" + idx + ", subject=" + subject + ", contents=" + contents + ", regDt=" + regDt
				+ ", userId=" + userId + ", etc=" + etc + ", hit=" + hit + "]";
	}
	
}
