package com.so.work.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.so.work.model.UserModel;

@Repository
public interface UserMapper {
	
	public List<UserModel> userList() throws Exception;
	
	public int insertUser(UserModel param) throws Exception;

}
