package com.so.work.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.so.work.model.BoardModel;

@Repository
public interface BoardMapper {
	
	public List<BoardModel> boardList() throws Exception;

}
